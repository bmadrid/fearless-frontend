function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="card shadow mt-3">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body text-start">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${start} - ${end}</div>
        </div>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const alert = document.getElementById('main-container');
            const html = '<div class="alert alert-danger" role="alert">Response from getting conference information was not good.</div>';
            alert.insertAdjacentHTML("afterbegin", html);
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const rawstart = new Date(details.conference.starts);
                    const rawend = new Date(details.conference.ends);
                    const format = { day: 'numeric', year: 'numeric', month: 'numeric' }
                    const start = rawstart.toLocaleDateString(format)
                    const end = rawend.toLocaleDateString(format)
                    const location = details.conference.location.name
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, start, end, location);
                    const row = document.querySelector('.card-columns');
                    row.innerHTML += html;
                }
            }
        }
    } catch (error) {
        const alert = document.getElementById('main-container');
        const html = '<div class="alert alert-danger" role="alert">An error occurred while trying to retrieve conference data.</div>';
        alert.insertAdjacentHTML("afterbegin", html);
    }
});
