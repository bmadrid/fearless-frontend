
window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(stateUrl);
        if (response.ok) {
            const data = await response.json();

            for (let state of data['states']) {
                const selection = document.getElementById('state');
                const newElement = document.createElement('option');
                newElement.value = state.abbreviation;
                newElement.innerText = state.name;
                selection.appendChild(newElement);
            }
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                    console.log(newLocation);
                }
            });

        } else {
            const alert = document.getElementById('main-container');
            const html = '<div class="alert alert-danger" role="alert">Response from getting conference information was not good.</div>';
            alert.insertAdjacentHTML("afterbegin", html);
        }
    } catch (error) {
        const alert = document.getElementById('main-container');
        const html = '<div class="alert alert-danger" role="alert">An error occurred while trying to retrieve conference data.</div>';
        alert.insertAdjacentHTML("afterbegin", html);
    }
});
