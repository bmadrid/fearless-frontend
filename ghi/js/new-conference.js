
window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(locationUrl);
        if (response.ok) {
            const data = await response.json();
            for (let location of data['locations']) {
                const selection = document.getElementById('location');
                const newElement = document.createElement('option');
                newElement.value = location.id;
                newElement.innerText = location.name;
                selection.appendChild(newElement);
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                } else {

                }
            });

        } else {
            const alert = document.getElementById('main-container');
            const html = '<div class="alert alert-danger" role="alert">Response from uploading conference information was not good.</div>';
            alert.insertAdjacentHTML("afterbegin", html);
        }
    } catch (error) {
        const alert = document.getElementById('main-container');
        const html = '<div class="alert alert-danger" role="alert">An error occurred while trying to retrieve conference data.</div>';
        alert.insertAdjacentHTML("afterbegin", html);
    }
});
